import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class RifleService {
    public activeMag: BehaviorSubject<any> = new BehaviorSubject(false);
    public activeRifle: BehaviorSubject<any> = new BehaviorSubject(false);
    public activeScope: BehaviorSubject<any> = new BehaviorSubject(false);

    // Hard code because lazy
    private ids = {
        mag: "Xt9nrFH5Mm6w6pp2mmsw",
        rifle: "ksb2odrnvhOweaf1llwZ",
        scope: "3HREGZplcSXHW5dGj72D"
    };

    constructor(private db: AngularFirestore) {
        db.collection('mag').valueChanges()
        .subscribe((mag: any) => {
            mag = mag[0];

            let tempMag = mag[mag.active];
            tempMag = tempMag ? tempMag : mag['30_round_mag'];

            this.activeMag.next(tempMag);
        });

        db.collection('rifle').valueChanges()
        .subscribe((rifle: any) => {
            rifle = rifle[0];

            let tempRifle = rifle[rifle.active];
            tempRifle = tempRifle ? tempRifle : rifle['m16'];

            this.activeRifle.next(tempRifle);
        });

        db.collection('scope').valueChanges()
        .subscribe((scope: any) => {
            scope = scope[0];

            let tempScope = scope[scope.active];
            tempScope = tempScope ? tempScope : scope['reddot'];

            this.activeScope.next(tempScope);
        });
    }

    setActive(event: any) {
        const type = event.type;
        const tag = event.tag;

        return this.db.collection(type).doc(this.ids[type]).update({active: tag});
    }

    printConfig() {

        if(this.activeMag.value && this.activeRifle.value && this.activeScope.value) {
            console.log(`Active magazine selection: ${this.activeMag.value.label}`);
            console.log(`Active rifle selection: ${this.activeRifle.value.label}`);
            console.log(`Active scope selection: ${this.activeScope.value.label}`);
        } else {
            console.log('Still loading data..');
        }
    }
}
