import { Component } from '@angular/core';
import { RifleService } from './rifle.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

    constructor(public RifleService: RifleService) {}
}
