import { Component, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core'

@Component({
  selector: 'app-rifle-config',
  templateUrl: './rifle-config.component.html',
  styleUrls: ['./rifle-config.component.scss']
})
export class RifleConfigComponent implements OnInit {
    @Output() update: EventEmitter<any> = new EventEmitter();
    @Output() confirm: EventEmitter<any> = new EventEmitter();

    constructor() { }

    ngOnInit() {
    }

    // TODO Should load available options from server, but lazy
}
