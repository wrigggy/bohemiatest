import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-rifle-display',
  templateUrl: './rifle-display.component.html',
  styleUrls: ['./rifle-display.component.scss']
})
export class RifleDisplayComponent implements OnInit {
    @Input() rifle: any;
    @Input() mag: any;
    @Input() scope: any;

    constructor() { }

    ngOnInit() {
    }
}
