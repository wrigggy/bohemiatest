import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';

import { AppComponent } from './app.component';
import { RifleDisplayComponent } from './rifle-display/rifle-display.component';
import { RifleConfigComponent } from './rifle-config/rifle-config.component';

import { RifleService } from './rifle.service';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../environments/environment';

@NgModule({
    declarations: [
        AppComponent,
        RifleDisplayComponent,
        RifleConfigComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        MatButtonModule,
        MatToolbarModule,
        MatCardModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule,
 //       AngularFireAuthModule
    ],
    providers: [
        RifleService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
